import './Event.css';
export const Event = ({ height, top, id, left, width }) => (
    <div className="Event"
      style={{
        height: `${height}%`,
        left: `${left}%`,
        top: `${top}%`,
        width: `${width}%`,
      }}
    >
      <p className="Title">{id}</p>
      <div className="Body"></div>
    </div>
  );
