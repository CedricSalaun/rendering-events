function checkOverlap(eventA, eventB) {
  const startA = parseTime(eventA.start);
  const endA = startA + eventA.duration;
  const startB = parseTime(eventB.start);
  const endB = startB + eventB.duration;

  return startA < endB && endA > startB;
}

export function parseTime(timeString) {
  if (!timeString) return;
  const [hours, minutes] = timeString.split(":").map(Number);
  return hours * 60 + minutes;
}

export function getOverlaps(event, collection, out, index) {
  return collection.reduce((acc, elem, jndex) => {
    if (!checkOverlap(event, elem)) return acc;
    if (jndex <= index) {
      acc.push(out.find(({id}) => id === elem.id) || elem);
    } else if (acc.every((slot) => checkOverlap(slot, elem))) {
      acc.push(elem);
    }
    return acc;
  }, []);
}

export function calculatePosition(event, group) {
  const position = group.findIndex(({id}) => id === event.id);

  let width = 100 / group.length;
  let left = 0;

  if (position) {
    const lastElem = group[position - 1];

    if (lastElem) {
      const lastElemWidth = lastElem.width;
      const lastElemLeft = lastElem.left;

      if (lastElemWidth + lastElemLeft + width <= 100) {
        left = lastElemWidth + lastElemLeft;
      } else if (left + width > lastElemLeft) {
        width = lastElemLeft;
      }
    }
  }

  return {width, left};
}
