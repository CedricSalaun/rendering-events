import './EventLayout.css';

import {Event} from '../Event/Event';
import timeSlots from '../../input.json';
import {parseTime, getOverlaps, calculatePosition} from './utils';

export const EventLayout = ({timeArray}) => {
  const [offset] = timeArray;
  const sortedTimeSlots = timeSlots.sort((a, b) => parseTime(a.start) - parseTime(b.start));
  const output = sortedTimeSlots.reduce((out, event, index, collection) => {
    const group = getOverlaps(event, collection, out, index);

    const top = ((parseTime(event.start) / 60) - offset) / timeArray.length * 100;
    const height = event.duration / (60 * timeArray.length / 100);
    const {left, width} = calculatePosition(event, group);
    return out.concat({...event, top, height, width, left});
  }, []);

  return (
    <div className="Event-container">
      {output.map(({id, ...props}) => <Event key={id} {...props} timeArray={timeArray} id={id}/>)}
    </div>
  );
};
