import React from 'react';

import './TableLayout.css';
import {EventLayout} from '../EventLayout/EventLayout';

const PERIODS = {timeStart: 8, timeEnd: 20};

const timeArray = Array.from(
  {length: PERIODS.timeEnd - PERIODS.timeStart + 1},
  (_, index) => index + PERIODS.timeStart,
);

const Line = ({number}) => (
  <div className="Line" style={{height: `calc(100% / ${timeArray.length})`}}>
    <div className="Box">
      <div>{number}</div>
      <div/>
    </div>
    <div className="Content">
      <div/>
      <div/>
    </div>
  </div>
);

const TableLayout = () => (
  <div className="Table-container">
    <>
      {timeArray.map((period) => <Line key={period} number={period}/>)}
      <EventLayout timeArray={timeArray}/>
    </>
  </div>
);

export default TableLayout;
