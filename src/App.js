import './App.css';
import TableLayout from './components/TableLayout/TableLayout';

function App() {
  return (
    <div className="App">
      <TableLayout />
    </div>
  );
}

export default App;
